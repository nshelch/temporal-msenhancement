% This code creates figures that show average preparation times for a chosen delay window
% This delay window of interest must be entered manually 
% timeDelayChoices[8] = {300, 400, 500, 550, 600, 650, 700, 800};
ms_prep_delay = [];
delay_choice = 800;
for i = 1:length(vt)
    if delay_time(i) == delay_choice
        ms_prep_delay = [ms_prep_delay ms_reaction_time(i)];
    end
end
 figure;
        histogram(ms_prep_delay,(0:50:2000));
        hold on
        p = plot([delay_choice, delay_choice], ylim, 'r-', 'LineWidth', 2);
        xlabel(sprintf('Microsaccade Preparation Time for %d ms delay trials [ms]',delay_choice))
        ylabel('Number of trials')
        legend(p,'Target On')
        print('-dbmp', sprintf('./Figures/%s_RGB%i_MSPrep_Delay_%d.bmp', params.Subject{sub_idx}, params.RGB(sub_idx),delay_choice))
        print('-depsc', sprintf('./Figures/%s_RGB%i_MSPrep_Delay_%d.eps', params.Subject{sub_idx}, params.RGB(sub_idx),delay_choice))
        