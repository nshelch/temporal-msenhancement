diary(sprintf('%s_RGB%i_summary', params.Subject{sub_idx}, params.RGB(sub_idx)))
diary ON

fprintf('Subject: %s\n', params.Subject{sub_idx})
% Summary Print Outs
fprintf('\nProportion of selected trials: %.2f\n', cont.valid_trials/length(vt))
fprintf('Prop correct cong: %.2f (n = %d) d prime: %.2f \n', ft.cong.avg_perf, cont.cong, stats.d_prime.d.cong)
fprintf('Prop correct neutral: %.2f (n = %d) d prime: %.2f \n', ft.neutral.avg_perf, cont.neutral, stats.d_prime.d.neutral)
fprintf('Prop correct incong: %.2f (n = %d) d prime: %.2f \n',ft.incong.avg_perf, cont.incong, stats.d_prime.d.incong)
fprintf('Prop correct congruent no saccade: %.2f (n = %d) d prime: %.2f\n', ft.cong_no_ms.avg_perf, cont.cong_no_ms,stats.d_prime.d.cong_no_ms)
fprintf('Prop correct incongruent no saccade: %.2f (n = %d) d prime: %.2f \n', ft.incong_no_ms.avg_perf, cont.incong_no_ms, stats.d_prime.d.incong_no_ms)

% Checking trial counts
fprintf('\n Trials discarded for blinks: %d', dt.blinks)
fprintf('\n Trials discarded for no track: %d', dt.no_track)
fprintf('\n Trials discarded for no response: %d', dt.no_response)
fprintf('\n Trials discarded for trial duration: %d', dt.too_short)
fprintf('\n Trials discarded for saccades: %d', dt.saccades)
fprintf('\n Trials discarded for early microsaccades: %d', dt.microsaccade)
fprintf('\n Trials discarded for multiple microsaccades: %d', dt.multiple_ms)
fprintf('\n Trials discaded for gaze off center: %d', dt.gaze_off_center)
fprintf('\n Trials discarded for target distance: %d', dt.target_distance)
fprintf('\n Trials discarded for late microsaccade: %d', dt.late_ms)
fprintf('\n Number of valid trials: %d', cont.valid_trials)

fprintf('\n\n Trials prior to filter: %d', ii)
fprintf('\n Total trials: %d \n', cont.valid_trials + sum(cell2mat(struct2cell(dt))))

diary OFF
movefile(sprintf('%s_RGB%i_summary', params.Subject{sub_idx}, params.RGB(sub_idx)), './Summaries')
