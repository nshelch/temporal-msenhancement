function GenerateMap(result, limit, pxAngle)

figure
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)

pcolor(linspace(-limit, limit, size(result, 1)), linspace(-limit, limit, size(result, 1)), result');

hold on
plot([-limit limit], [0 0], '--k')
plot([0 0], [-limit limit], '--k')
plot((40* pxAngle), 0, 'sk', 'MarkerSize', 10,'MarkerFaceColor', 'w')
plot((-40* pxAngle), 0, 'sk', 'MarkerSize', 10,'MarkerFaceColor', 'w')

set(gca, 'FontSize', 16) 
xlabel('X [arcmin]')
ylabel('Y [arcmin]')
axis tight
axis square
caxis([0 1])
%axis off
box off
shading interp;

clear figure

