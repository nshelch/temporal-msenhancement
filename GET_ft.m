clc; clear; close all;

params.Subject = {'NS'};
params.RGB = 90;

params.sampleConversion = 1000/330;
params.DDPI = 0;
params.FIGURES = 1;
params.TABLES = 0;
params.ACROSS_SUBJECT_TABLE = 0;
params.tempDynBins = 4;

for sub_idx = 1:length(params.Subject)
   
    if params.DDPI
        load(sprintf('./Data/%s_RGB%i-DDPI.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
    else
        load(sprintf('./Data/%s_RGB%i.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
    end
    
    DelayAfterTOn = 450;
    Dist_thresh = 18;
    DistTarget_tresL = 10;
    DistTarget_tresU = 30;
    MaxMsAmp = 30;
    MinMsAmp = 0;
    
    trial_types = {'cong', 'incong', 'neutral', 'cong_no_ms', 'incong_no_ms', ...
        'neutral_cong', 'neutral_incong', 'cong_invalid', 'incong_valid', 'other'};

    for trial_idx = 1:length(trial_types)
        cur_trial = trial_types{trial_idx};
        cont.(cur_trial) = 0;
        ft.(cur_trial) = filteredTrialsStruct();
        ft.(cur_trial).heatmap(1).xx = [];
        ft.(cur_trial).heatmap(1).yy = [];
    end
    
    % Initialize counters
    cont.valid_trials = 0; % total microsaccade trials (that passed the filter)
    cont.filtered_trials = 0; % total trials that passed filter
    
    % Initialize vectors
    [ms_reaction_time, orientation_cued_target] = deal(NaN(1, length(vt)));
    
    % Initalize discarded trials struct
    dt = discardedTrialsStruct();
   
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    for ii = 1:length(vt)
        % Saves cued target orientation
        if vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 135
            orientation_cued_target(ii) = 0;
        elseif vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 45
            orientation_cued_target(ii) = 1;
        end
        
        % Finds all microsaccades that occured after saccade cue signal
        saccade_cue_on = vt{ii}.TimeSaccCueON;
        idx_valid_ms = find(vt{ii}.microsaccades.start > saccade_cue_on);
        
        % first microsaccade after the saccade cue signal in all trials
        if ~isempty(idx_valid_ms)
            %ms_reaction_time is a vector that contains the time is took
            %the subject to make a microsaccade after the saccade cue onset
            ms_reaction_time(ii) = (vt{ii}.microsaccades.start(idx_valid_ms(1)) - saccade_cue_on );
        end
        
        % eliminate trials with no tracks, blinks, large saccades, no response,
        % and too short trial duration
        [goodTrial, dt] = filter_trial(vt{ii}, dt, params);
        if goodTrial
            
            % Trials in which the subject responded and passed a filter
            cont.filtered_trials = cont.filtered_trials + 1;
            
            % eliminates microsaccades that occurred at the very beginning
            % of the trial and were used to recenter the gaze
            id = find(vt{ii}.microsaccades.start > 50);
            ms_start = vt{ii}.microsaccades.start(id);
            ms_end = vt{ii}.microsaccades.duration(id) + ms_start;
            ms_amp = vt{ii}.microsaccades.amplitude(id);
            
            % Since we are interested in the temporal dynamics of ms
            % preparation, we want to look at the entire interval from sacc
            % cue onset to response cue. As long as 1 microsaccade is
            % performed in that time interval, it can be used for analysis.
            idx_valid_ms = isBetween(saccade_cue_on, ms_start, vt{ii}.TimeRespCueON);
            num_valid_ms = length(idx_valid_ms);
            
            % check that the average gaze position when the target is on is
            % close to 0
            target_on = round(vt{ii}.TimeTargetON);
            target_off = round(vt{ii}.TimeTargetOFF);
            if params.DDPI
                start_time = round((target_on - 50) / (1000/330));
                end_time = round(target_off / (1000/330));
                xx_target = vt{ii}.x.position(start_time:end_time) + vt{ii}.xoffset * vt{ii}.pxAngle;
                yy_target = vt{ii}.y.position(start_time:end_time) + vt{ii}.yoffset * vt{ii}.pxAngle;
            else
                xx_target = vt{ii}.x.position((target_on - 50):target_off) + vt{ii}.xoffset * vt{ii}.pxAngle;
                yy_target = vt{ii}.y.position((target_on - 50):target_off) + vt{ii}.yoffset * vt{ii}.pxAngle;
            end
            
            % gaze location
            gaze_loc = mean(sqrt(xx_target.^2 + yy_target.^2));
            ft.gaze_during_target_x(ii) = mean(xx_target);
            ft.gaze_during_target_y(ii) = mean(yy_target);
            
            % distance from the cued location
            dist_from_target = target_classification(xx_target, yy_target, vt{ii}.CueLocation, vt{ii}.TargetOffsetpx, vt{ii}.pxAngle);
            
            % IF A MICROSACCADE HAS BEEN PERFORMED
            if num_valid_ms <= 1 && ...
                    mean(gaze_loc) < Dist_thresh &&...
                    mean(dist_from_target) > DistTarget_tresL && ...
                    mean(dist_from_target) < DistTarget_tresU
                
                if num_valid_ms == 1 && ...
                        ms_amp(idx_valid_ms(1)) < MaxMsAmp && ...
                        ms_amp(idx_valid_ms(1)) > MinMsAmp
                    
                    % Trials in which a microsaccade was performed
                    msId = find(vt{ii}.microsaccades.amplitude == ms_amp(idx_valid_ms(1)));
                    [ft, cont, ~] = insert_trial_data(ft, params, cont, [], vt, ii, msId, xx_target, yy_target);
                    
                    % NO MICROSACCADE PERFORMED
                elseif num_valid_ms == 0
                    msId = [];
                    [ft, cont, ~] = insert_trial_data(ft, params, cont, [], vt, ii, msId, xx_target, yy_target);
                else
                    dt.late_ms = dt.late_ms + 1;
                end
            else
                if num_valid_ms > 1
                    dt.multiple_ms = dt.multiple_ms + 1;
                elseif ~(mean(gaze_loc) < Dist_thresh)
                    dt.gaze_off_center = dt.gaze_off_center + 1;
                elseif ~(mean(dist_from_target) > DistTarget_tresL) || ~(mean(dist_from_target) < DistTarget_tresU)
                    dt.target_distance = dt.target_distance + 1;
                end
            end
        end
    end
   
    subj_resp = cellfun(@(z) z(:).Response, vt);
    sacc_cue_type = cellfun(@(z) z(:).SaccCueType, vt);
    cue_location = cellfun(@(z) z(:).CueLocation, vt);
    delay_time = cellfun(@(z) z(:).DelayTime, vt); % Subtract 100 because the timer starts with the saccade cue timer
    
    % Check this
    response_time = cellfun(@(z) z(:).ResponseTime - z(:).TimeRespCueON - z(:).RespCueTime, vt);
    
    trial_types = {'cong','incong','neutral','cong_no_ms','incong_no_ms'};
    for trial_idx = 1:length(trial_types)
        cur_trial = trial_types{trial_idx};
        if ~isnan(ft.(cur_trial).perf)
            ft.(cur_trial).avg_perf = sum(ft.(cur_trial).perf)/length(ft.(cur_trial).perf);
            [stats.d_prime.d.(cur_trial), ~, stats.d_prime.ci.(cur_trial), stats.d_prime.crit.(cur_trial), stats.d_prime.var.(cur_trial)] = ...
                CalculateDprime_2(subj_resp(ft.(cur_trial).id), orientation_cued_target(ft.(cur_trial).id));
            ft.(cur_trial).dprime = stats.d_prime.d.(cur_trial);
        else
            ft.(cur_trial).avg_perf = NaN;
            [stats.d_prime.d.(cur_trial), ~, stats.d_prime.ci.(cur_trial), stats.d_prime.crit.(cur_trial), stats.d_prime.var.(cur_trial)] = deal(NaN);
            ft.(cur_trial).dprime = NaN;
        end
    end
    