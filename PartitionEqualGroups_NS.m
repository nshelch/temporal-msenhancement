%
% [bin nn Thresholds AxisNum] = PartitionEqualGroups(xx, NInt)
%
% Partition variable xx into NInt groups with equal number of samples
% bin(i) is the group (1:NInt) to which the value xx(i) belongs
% nn are the number of samples in each bin
% Thresholds are the boundaries of each group
% AxisNum are the means of each group
function [bin nn Thresholds AxisNum] = PartitionEqualGroups(xx, NInt)

% find Nint intervals with equal numbers of samples
[dd SortedIndeces] = sort(xx);
RelIndeces = round([1:NInt-1]*(length(dd)/NInt+1));
Intervals = dd(RelIndeces);

%     % Numbers to plot on axis (half intervals)
%     AxisNum = zeros(1,NInt);
%     AxisNum(1) = Intervals(1)/2;
%     for ii = 2:NInt-1;
%         AxisNum(ii) = AxisNum(ii-1)+Intervals(ii)-Intervals(ii-1);
%     end
%     AxisNum(NInt) = AxisNum(NInt-1)+(max(dd)-Intervals(NInt-1))/2;

% find samples in selected intervals and count decisions
minThresh = -Inf;
maxThresh = Inf;

Thresholds = [minThresh Intervals maxThresh];
[nn bin] = histc(xx, Thresholds);
nn = nn(1:end-1);

% Numbers to plot on axis
AxisNum = zeros(1,NInt);
for ii =1:NInt
    AxisNum(ii) = mean(xx(bin == ii));
end