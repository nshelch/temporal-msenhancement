function [trial_type, counter] = trial_type_classification(ms, xPos, cueLocation, saccCue, counter)

trial_type = 'other';
if ms
    if saccCue ~= 0
        % Sacc Cue == Response Cue && Ms Dir == Sacc Cue
        if gaze_pos_sacc_cue_match(xPos, saccCue) && (saccCue == cueLocation)
            trial_type = 'cong';
        % Sacc Cue != Response Cue && Ms Dir == Sacc Cue
        elseif gaze_pos_sacc_cue_match(xPos, saccCue) && (saccCue ~= cueLocation)
            trial_type = 'incong';
        % Sacc Cue == Response Cue && Ms Dir != Sacc Cue
        elseif ~gaze_pos_sacc_cue_match(xPos, saccCue) && (saccCue == cueLocation)
            trial_type = 'cong_invalid';
        % Sacc Cue != Response Cue && Ms Dir == Response Cue
        elseif gaze_pos_resp_cue_match(xPos, cueLocation) && (saccCue ~= cueLocation)
            trial_type = 'incong_valid';
        end
    elseif saccCue == 0
        % Neutral Sacc Cue && Ms Dir == Response Cue
        if gaze_pos_resp_cue_match(xPos, cueLocation)
            trial_type = 'neutral_cong';
        % Neutral Sacc Cue && Ms Dir != Response Cue
        elseif ~gaze_pos_resp_cue_match(xPos, cueLocation)
            trial_type = 'neutral_incong';
        end
    end
else
    % Sacc Cue == Response Cue && NO Microsaccade
    if cueLocation == saccCue
        trial_type = 'cong_no_ms';
    % Neutral Sacc Cue && NO Microsaccade
    elseif saccCue == 0
        trial_type = 'neutral';
    % Sacc Cue != Response Cue && NO Microsaccade
    elseif cueLocation ~= saccCue
        trial_type = 'incong_no_ms';
    end
end

counter.(trial_type) = counter.(trial_type) + 1;

end

% Check if the direction of the microsaccade matches the sacc cue direction
function output = gaze_pos_sacc_cue_match(gazePos, saccCue)

if (saccCue == 1 && gazePos < 0) || ...
        (saccCue == 2 && gazePos > 0)
    output = 1;
else
    output = 0;
end

end

% Check if the direction of the microsaccade matches the resp cue direction
function output = gaze_pos_resp_cue_match(gazePos, respCue)

if (respCue == 1 && gazePos < 0) || ...
        (respCue == 2 && gazePos > 0)
    output = 1;
else
    output = 0;
end

end
