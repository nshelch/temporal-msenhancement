
for ii = 804:length(vt)
% creates a plot of the eye trace with cue onset and target onset tag
    figure;
    Fs = 330; 
    tm = [0:length(vt{ii}.x.position)-1] .* 1/Fs * 1000;
    plot(tm,vt{ii}.x.position); hold on; 
    plot(tm,vt{ii}.y.position); hold on; 
    axis tight
    plot([vt{ii}.TimeTargetON,vt{ii}.TimeTargetON],ylim, 'y'); hold on;
    plot([vt{ii}.TimeSaccCueON,vt{ii}.TimeSaccCueON],ylim, 'g')
    text(vt{ii}.TimeTargetON, 0, '\leftarrow Target On' )
    text(vt{ii}.TimeSaccCueON, 0, '\leftarrow Cue On' )
    delay = (vt{ii}.TimeTargetON - vt{ii}.TimeSaccCueON);
    set(gca,'TickDir','out');
    xlabel('Time (ms)')
    ylabel('Arcmin')
    title(sprintf('Subject: ZS RGB: 50 Trial Num:%d Delay: %d ms', ii, floor(delay)))
    
end