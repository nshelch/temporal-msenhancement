%% Accuracy

% Peripheral Conditon
figure('Position', [500, 200, 700, 700]);
v_perf = [ft.cong.avg_perf, ft.neutral.avg_perf, ft.incong.avg_perf, ft.incong.avg_perf, ft.incong.avg_perf];
std_perf = [sqrt((ft.cong.avg_perf * abs(ft.cong.avg_perf - 1)) / length(ft.cong.perf)), ...
    sqrt((ft.neutral.avg_perf * abs(ft.neutral.avg_perf - 1)) / length(ft.neutral.perf)), ...
    sqrt((ft.incong.avg_perf * abs(ft.incong.avg_perf - 1)) / length(ft.incong.perf)), ...
    sqrt((ft.cong_no_ms.avg_perf * abs(ft.cong_no_ms.avg_perf - 1)) / length(ft.cong_no_ms.perf)), ...
    sqrt((ft.incong_no_ms.avg_perf * abs(ft.incong_no_ms.avg_perf - 1)) / length(ft.incong_no_ms.perf))];
hold on
errorbar(v_perf, std_perf, 'ok','LineStyle', 'none', 'LineWidth',2)
title('Performance')
set(gca, 'FontSize', 15, 'YTick', 0:.1:1)
xticks(1:5)
xticklabels({'Cong','Neutral','Incong','Fix. Cong','Fix. Incong'})
ylabel('Proportion Correct')
ylim([0, 1])
xlim([.5 5.5])
box off
print('-djpeg',sprintf('./Documents/Figures/%s_%i_Performance.jpeg', Subject{sub_idx}, rgb_value(sub_idx)));

%% D-prime

% Peripheral Condition
figure('Position', [500, 200, 700, 700]);
v_dprime = [stats.d_prime.d.cong, stats.d_prime.d.neutral, ...
    stats.d_prime.d.incong, stats.d_prime.d.cong_no_ms, stats.d_prime.d.incong_no_ms];
ci_dprime = [stats.d_prime.ci.cong, stats.d_prime.ci.neutral, ...
    stats.d_prime.ci.incong, stats.d_prime.ci.cong_no_ms, stats.d_prime.ci.incong_no_ms];
errorbar(v_dprime, ci_dprime, 'ok','LineStyle', 'none', 'LineWidth',2)
set(gca, 'FontSize', 15, 'YTick', -10:1:10)
xticks(1:5)
xticklabels({'Cong','Neutral','Incong','Fix. Cong','Fix. Incong'})
xlim([.5 5.5])
ylabel('Sensitivity [d'']')
% ylim([min(v_dprime) - 1.5, 5])
ylim([floor(min(v_dprime) - max(ci_dprime)), ceil(max(v_dprime) + max(ci_dprime))])
box off

print('-djpeg',sprintf('./Documents/Figures/%s_%i_DPrime.jpeg', Subject{sub_idx}, rgb_value(sub_idx)));

%% Gaze Position During Target Presentation

% Combined by Saccade Cue
figure('Position', [500, 200, 700, 700]);
hold on
h = plot(0,0,'g','Linewidth', 5); j = plot(0,0,'c','Linewidth', 5); k = plot(0,0,'m','Linewidth', 5);
q = plot_stimuli_location(0, 11*pxAngle, 30*pxAngle);
hold on
color_vector = ['g','c','m'];
id = 1;
for sacc_cue = [0,1,2]
    overlap = intersect(find(ft.sacc_cue_type == sacc_cue), ft.id_ms_trials);
    q = plot(ft.gaze_during_target_x(overlap), ft.gaze_during_target_y(overlap), 'o', ...
        'MarkerFaceColor', color_vector(id), 'MarkerEdgeColor', 'none','MarkerSize',4);
    id = id + 1;
end
% Plot boundry line for distance threshold (distance from center)
viscircles([0,0],Dist_thresh/2)
set(q, 'HandleVisibility', 'off')
legend([h,j,k], {'Neutral','Sacc. Left','Sacc. Right'})
title('Distance from target (Combined)')
print('-djpeg',sprintf('./Documents/Figures/%s_%i_distanceFromTargetLocation.jpeg', Subject{sub_idx}, rgb_value(sub_idx)));

%% Histogram of Microsaccade Latencies

figure('Position', [500, 200, 700, 700]);
histogram([ft.cong.ms_latency, ft.incong.ms_latency],'normalization','probability')
title('Microsaccade Latency Distribution')
xlabel('Time from target onset')
ylabel('Probability')
set(gca, 'FontSize', 15)
