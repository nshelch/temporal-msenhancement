function dist = target_classification(x, y, location, targetOffset, pxAngle)

switch location
    case 1 % Left target
        dist = sqrt((x - (-double(targetOffset) * pxAngle)).^2 + (y.^2));
    case 2 % Right target
        dist = sqrt((x - (double(targetOffset) * pxAngle)).^2 + (y.^2));
end

end

