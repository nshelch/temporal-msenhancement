function [dataStruct, counter, heatmap] = insert_trial_data(dataStruct, params, counter, heatmap, vt, ii, msId, xx_target, yy_target)

if params.DDPI
    msStart = vt{ii}.microsaccades.startSample;
    msEnd = msStart + vt{ii}.microsaccades.durationSample;
else
    msStart = vt{ii}.microsaccades.start;
    msEnd = msStart + vt{ii}.microsaccades.duration;
end

msAmp = vt{ii}.microsaccades.amplitude;
saccCueOn = round(vt{ii}.TimeSaccCueON);
targetOn = vt{ii}.TimeTargetON;
targetOff = round(vt{ii}.TimeTargetOFF);
targetOffset = double(vt{ii}.TargetOffsetpx);
respCueLoc = vt{ii}.CueLocation;
saccCueLoc = vt{ii}.SaccCueType;
xOffset = vt{ii}.xoffset * 0.6848;
yOffset = vt{ii}.yoffset * 0.6848;
counter.valid_trials = counter.valid_trials + 1;
dataStruct.valid_trial_id(counter.valid_trials) = ii;
distFromTarget = target_classification(xx_target, yy_target, respCueLoc, targetOffset, 0.6848);


if isempty(msId) % no ms trials
    [trialType, counter] = trial_type_classification(0, NaN, respCueLoc, saccCueLoc, counter);
    trialCount = counter.(trialType);
else
    dataStruct.ms_dir_vec_x(ii) = vt{ii}.x.position(msEnd(msId)) - vt{ii}.x.position(msStart(msId));
    dataStruct.ms_dir_vec_y(ii) = vt{ii}.y.position(msEnd(msId)) - vt{ii}.y.position(msStart(msId));
    
    [trialType, counter] = trial_type_classification(1, dataStruct.ms_dir_vec_x(ii), respCueLoc, saccCueLoc, counter);
    trialCount = counter.(trialType);
    
    dataStruct.ms_end_xpos(ii) = vt{ii}.x.position(msEnd(msId))+ xOffset;
    dataStruct.ms_end_ypos(ii) = vt{ii}.y.position(msEnd(msId))+ yOffset;
    dataStruct.landing_error(ii) = target_classification(dataStruct.ms_end_xpos(ii), dataStruct.ms_end_ypos(ii), respCueLoc, targetOffset, 0.6848);
    dataStruct.(trialType).amp(trialCount) = msAmp(msId);
    
    if params.DDPI
        dataStruct.(trialType).ms_latency(trialCount) = vt{ii}.microsaccades.start(msId) - targetOn;
    else
        dataStruct.(trialType).ms_latency(trialCount) = msStart(msId) - targetOn;
    end
    dataStruct.(trialType).landing_error(trialCount) = dataStruct.landing_error(ii);
    
    saccLandingTimeInterval = msEnd(msId):msEnd(msId) + 50; %msEnd(msId) - 25:msEnd(msId) + 25;
end

dataStruct.(trialType).perf(trialCount) = vt{ii}.Correct;
dataStruct.(trialType).id(trialCount) = ii;
dataStruct.(trialType).gaze_pos(trialCount) = mean(sqrt(xx_target.^2 + yy_target.^2));
dataStruct.(trialType).dist_from_target{trialCount} = distFromTarget;
dataStruct.(trialType).response_time(trialCount) = vt{ii}.ResponseTime - vt{ii}.TimeRespCueON; % Response Time - Time Response Cue ON
dataStruct.(trialType).subj_resp(trialCount) = vt{ii}.Response;

if vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 135
    dataStruct.(trialType).orientation_cued_target(trialCount) = 0;
elseif vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 45
    dataStruct.(trialType).orientation_cued_target(trialCount) = 1;
end

% % Drift
% driftStart = targetOn;
% 
% if strcmp(trialType, 'neutral') || strcmp(trialType, 'cong_no_ms') || ...
%         strcmp(trialType, 'incong_no_ms')
%     driftEnd = targetOn + 500;
% else
%     driftEnd = msStart(id);
% end
% if params.DDPI
%     start_time = round(driftStart/params.sampleConversion);
%     end_time = round(driftEnd/params.sampleConversion);
%     x = vt{ii}.x.position(start_time:end_time) + xOffset;
%     y = vt{ii}.y.position(start_time:end_time) + yOffset;
% else
%     x = vt{ii}.x.position(round(driftStart):round(driftEnd)) + xOffset;
%     y = vt{ii}.y.position(round(driftStart):round(driftEnd)) + yOffset;
% end
% dataStruct = insert_drift_data(dataStruct, trialType, trialCount, x, y);

end


