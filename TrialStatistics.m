for ii = 1:length(subjects)
    load(sprintf('./Data/%s_summary.mat',subjects{ii}))
    indv.cont_c(ii) = ft.num_cong_trials;
    indv.cont_i(ii) = ft.num_incong_trials;
    indv.cont_n(ii) = ft.num_neutral_trials;
    indv.bad_trial(ii) = discarded_trials.bad_trial;
end

indv.total_trials(ii) = indv.cont_c + indv.cont_n + 
perc_discarded_trials = mean(indv.bad_trial./indv.total_trials) * 100;