clc; close all; clear;

Subject = {'ZS'};
rgb_value = (50);
MaxMSaccAmp = 30;
DDPI =1;

for ii = 1:length(Subject)
if DDPI
    pt = sprintf('./UnprocessedData/%s/RGB%i-DDPI/', Subject{ii}, rgb_value(ii));
    data = readdata2(pt, List);
    vt = preprocessingDDPI(data, MaxMSaccAmp, 330);
    vt = convertSamplesToMs(vt, 1000/330);
    vt = cellfun(@(x) setfield(x, 'pxAngle', 0.6848), vt, 'UniformOutput', false);
    save(sprintf('../Data/%s_RGB%i-DDPI.mat', Subject{ii}, rgb_value(ii)), 'vt')
else
    pt = sprintf('./UnprocessedData/%s/RGB%i', Subject{ii}, rgb_value(ii));
    data = readdata2(pt, List);
    vt = preprocessing(data, MaxMSaccAmp);
    vt = cellfun(@(x) setfield(x, 'pxAngle', 0.6848), vt, 'UniformOutput', false);
    save(sprintf('../Data/%s_RGB%i.mat',Subject{ii}, rgb_value(ii)), 'vt')
end
end