function list = List()

% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

list = eis_readData([], 'x');
list = eis_readData(list, 'y');

list = eis_readData(list, 'trigger', 'blink');
list = eis_readData(list, 'trigger', 'notrack');

% list = eis_readData(list, 'stream', 0, 'double'); %%
% list = eis_readData(list, 'stream', 1, 'double'); %%

        
%----------------------------------------------------------
%Test Calibration
list = eis_readData(list, 'uservar','TestCalibration');
list = eis_readData(list, 'uservar','xoffset');
list = eis_readData(list, 'uservar','yoffset');

%time of the response (locked to the start of the trial)	
list = eis_readData(list, 'uservar','TimeFixationON');
list = eis_readData(list, 'uservar','TimeFixationOFF');
list = eis_readData(list, 'uservar','TimeDelayON');
list = eis_readData(list, 'uservar','TimeDelayOFF');
list = eis_readData(list, 'uservar','TimeSaccCueON');
list = eis_readData(list, 'uservar','TimeSaccCueOFF');
list = eis_readData(list, 'uservar','TimeTargetON');
list = eis_readData(list, 'uservar','TimeTargetOFF');
list = eis_readData(list, 'uservar','TimeMaskON');
list = eis_readData(list, 'uservar','TimeMaskOFF');
list = eis_readData(list, 'uservar','TimeRespCueON');
list = eis_readData(list, 'uservar','TimeRespCueON');

%duration of events (ms)		
list = eis_readData(list, 'uservar','FixationTime');
list = eis_readData(list, 'uservar','DelayTime');
list = eis_readData(list, 'uservar','SaccTime');
list = eis_readData(list, 'uservar','TargetTime');
list = eis_readData(list, 'uservar','MaskTime');
list = eis_readData(list, 'uservar','RespCueTime');

% response
list = eis_readData(list, 'uservar','Correct');

% target orientation
list = eis_readData(list, 'uservar','Target1Orientation');
list = eis_readData(list, 'uservar','Target2Orientation');

% target size px
list = eis_readData(list, 'uservar','TSizeY');
list = eis_readData(list, 'uservar','TSizeX');

% target offset
list = eis_readData(list, 'uservar','XlocationPx');
list = eis_readData(list, 'uservar','YlocationPx');

% target rgb color
list = eis_readData(list, 'uservar','TargetRGB');

% stimuli size
list = eis_readData(list, 'uservar','FixSize');
list = eis_readData(list, 'uservar','BoxSize');

% cue location
list = eis_readData(list, 'uservar','CueLocation');
list = eis_readData(list, 'uservar','Response');	
list = eis_readData(list, 'uservar','ResponseTime');
list = eis_readData(list, 'uservar','TargetOffsetpx'); % px

% 0 neutral, 1 R and 2 L
list = eis_readData(list, 'uservar','SaccCueType');

% monitor variables
list = eis_readData(list, 'uservar','RefreshRate');
list = eis_readData(list, 'uservar','Xres');
list = eis_readData(list, 'uservar','Yres');

list = eis_readData(list, 'uservar','Subject_Name');
%----------------------------------------------------------
