#include "stdafx.h"
#include "ExperimentBody.h"
#include "MicrosaccadeEnhancement.h"
#include <math.h>

const float PI = 3.1415926353f;



///////////////////////////////////////////////////////////////////////////////////
ExperimentBody::ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params) :
	CExperiment(pxWidth, pxHeight, RefreshRate)
	
{
	setExperimentName("Temporal-MicrosaccadeEnhancement");
	m_paramsFile = Params;
	CMath::srand(time(NULL));
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::initialize()
{		
	CStabilizer::Instance()->enableSlowStabilization(true);
	
	totalNeutral = 0;
	totalTrials = 0;
	totalLeft = 0;
	totalRight = 0;

	// locations of boxes and targets
	TargetOffset = m_paramsFile->getInteger(CFG_TARGET_OFFSET);
	FixSize = m_paramsFile->getInteger(CFG_FIXSIZE);

	// Fixation point for trial (vertical line)
	m_cue_fixation = addObject(new CSolidPlane(0, 0, 0));
	m_cue_fixation->pxSetSize(FixSize, FixSize);
	m_cue_fixation->pxSetPosition(0, 0);
	m_cue_fixation->hide();

	// Response Cue
	m_cue_resp = addObject(new CImagePlane("images/cue_big.tga"));
	m_cue_resp->enableTrasparency(true);
	m_cue_resp->pxSetPosition(0,0);
	m_cue_resp->degSetAngle(90);
	//m_cue->pxSetSize(13, 13);
	m_cue_resp->hide();
	
	// Box labels: 
	// 1	2
	BoxSize = m_paramsFile->getInteger(CFG_BOXSIZE);
	m_box1 = addObject(new CSolidPlane(150, 150, 150));
	m_box1->pxSetSize(BoxSize,BoxSize);
	m_box2 = addObject(new CSolidPlane(150, 150, 150));
	m_box2->pxSetSize(BoxSize, BoxSize); 

	m_box1->pxSetPosition(-TargetOffset, 0);
	m_box2->pxSetPosition(TargetOffset, 0);

	m_box1->hide();
	m_box2->hide();

	// saccade cue
	m_cue_sacc = addObject(new CImagePlane("images/new_Sacc_cue.tga"));
	m_cue_sacc->enableTrasparency(true);
	m_cue_sacc->pxSetSize(25, 18);
	m_cue_sacc->pxSetPosition(0, 0);
	m_cue_sacc->hide();

	m_neutralcue_sacc = addObject(new CImagePlane("images/new_Sacc_neutralcue.tga"));
	m_neutralcue_sacc->enableTrasparency(true);
	m_neutralcue_sacc->pxSetSize(18, 18);
	m_neutralcue_sacc->pxSetPosition(0, 0);
	m_neutralcue_sacc->hide();

	// target is a small bar
	TSizeX = m_paramsFile->getInteger(CFG_TSIZEX);
	TSizeY = m_paramsFile->getInteger(CFG_TSIZEY);
	
	m_target1 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target1->pxSetSize(TSizeX, TSizeY);
	m_target1->pxSetPosition(-TargetOffset, 0);
	m_target1->hide();

	m_target2 = addObject(new CSolidPlane(m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE), m_paramsFile->getFloat(CFG_RGB_VALUE)));
	m_target2->pxSetSize(TSizeX, TSizeY);
	m_target2->pxSetPosition(TargetOffset, 0);
	m_target2->hide();

	// performance keepers
	totalCorrect = 0;
	totalResponses = 0;
	neutralCorrect = 0;

	// set the timers
	m_fixationTime = m_paramsFile->getInteger(CFG_FIXATION_TIME);
	m_targetTime = m_paramsFile->getInteger(CFG_TARGET_TIME);
	m_saccTime = m_paramsFile->getInteger(CFG_SACCCUE_TIME);
	m_respcueTime = m_paramsFile->getInteger(CFG_RESPCUE_TIME);
	m_responseTime = m_paramsFile->getInteger(CFG_RESPONSE_TIME);
	m_maskTime = m_paramsFile->getInteger(CFG_MASK_TIME);
	
	// set the pixel increment for the test calibration procedure during the exp
	// NOTE: smaller increments (in px) more precise is the recalibration but it will also take more time
	Increment = 1;
	ResponseFinalize = 0;
	xshift = 0;
	yshift = 0;
	xPos = 0;
	yPos = 0;
	TrialNumber = 1;
	m_numTestCalibration = 0;
	
	// set TestCalibration = 1 so that the experiment will start with a recalibration trial
	TestCalibration = 1;
	
	// boxes for the recalibration trials	
	m_whitecross = addObject(new CSolidPlane(255, 255, 255));
	m_whitecross->pxSetSize(10,10);
	m_blackcross = addObject(new CSolidPlane(0, 0, 0));
	m_blackcross->pxSetSize(10,10);

	disable(CExperiment::EIS_JP_STRT);
	disable(CExperiment::EIS_PHOTOCELL);
	disable(CExperiment::EIS_NOTRACK_ICON);
	disable(CExperiment::EIS_STAT1);
	
	
	hideAllObjects();
	m_state = STATE_LOADING;
	m_timer.start(1000);

	WAIT_RESPONSE = 1;
	
	/* Seed the random-number generator with current time so that
    * the numbers will be different every time we run.*/
	
    srand( (unsigned)time( NULL ) );


}
///////////////////////////////////////////////////////////////////////////////////
// write the progress file
void ExperimentBody::finalize()
{
			declareFinished();
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventRender(unsigned int FrameCount, CEOSData* Samples)
{

	float x;
	float y;

	switch (m_state) {
	case STATE_LOADING:


		COGLEngine::Instance()->clearScreen();
		glColor3d(255, 255, 255);

		// Set the background color for the experiment
		COGLEngine::Instance()->setBackgroundColor(127,127,127);

		// Copy the parameter file into the subject directory
		if (m_timer.isExpired())
		{
			char LocalDate[1024];
			time_t t = time(NULL);
			strftime(LocalDate, 1024, "%Y-%m-%d-%H-%M-%S", localtime(&t));

			ostringstream DestinationFileName;
			DestinationFileName << m_paramsFile->getDirectory(CFG_DATA_DESTINATION) <<
				m_paramsFile->getString(CFG_SUBJECT_NAME) << "/" << m_paramsFile->getString(CFG_SUBJECT_NAME) <<
				"-" << LocalDate << "-params.cfg";;
			gotoFixation();
		}

		break;

	case STATE_TESTCALIBRATION:

		m_box1->hide();
		m_box2->hide();

		if (!m_timerCheck.isExpired())
		{
			CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Test Calibration, ResponseFinalize: %.0i", ResponseFinalize);
			if (!(ResponseFinalize == 1))
			{
				CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
				//CEnvironment::Instance()->outputMessage("State Test Calibration, %1.2f, %1.2f", x, y);
				moveToFront(m_blackcross);
				m_blackcross->pxSetPosition(x + xshift + xPos, y + yshift + yPos);
				//m_redcross->pxSetPosition(x, y);
				m_blackcross->show();
				m_whitecross->pxSetPosition(0, 0);
				m_whitecross->show();
			}
			else
			{
				//CEnvironment::Instance()->outputMessage("State Test Calibration");
				TestCalibration = 0;

				xshift = xPos + xshift;
				yshift = yPos + yshift;
				//CEnvironment::Instance()->outputMessage("State Test Calibration, xshift: %.2f", xshift);
				//CEnvironment::Instance()->outputMessage("State Test Calibration, yshift: %.2f", yshift);
				//CEnvironment::Instance()->outputMessage("----------------------------------------------------------------");


				m_blackcross->hide();
				m_whitecross->hide();
				proceedToNextState = true;

				endTrial();
				gotoFixation();

			}

		}
		break;

		//  STATE_FIXATION: the place holders are presented together with the central fixation point.
	case STATE_FIXATION:
		if (gate == 1)
		{
			CConverter::Instance()->a2p(Samples->x1, Samples->y1, Xlocation, Ylocation);
			//CEnvironment::Instance()->outputMessage("State Fixation, %1.2f, %1.2f", Xlocation, Ylocation);
			
			// to debug
			Xlocation = 0;
			Ylocation = 0;

			
			// Add fixation point
			m_cue_fixation->show();


			m_box1->pxSetPosition(-TargetOffset, 0);
			m_box2->pxSetPosition(TargetOffset, 0);
			m_box1->show();
			m_box2->show();
			moveToFront(m_box1);
			moveToFront(m_box2);


			// time for fixation ON
			TimeFixationON = m_timerExp.getTime();
			//CEnvironment::Instance()->outputMessage("State Fixation");
			// fixation should stay on for one sec or as far as the gaze is stabilized
			m_timerFixation.start(m_fixationTime);
			gate = 0;
		}
		

		if (m_timerFixation.isExpired()){
			//CEnvironment::Instance()->outputMessage("Fixation OVER");
			TimeFixationOFF = m_timerExp.getTime();
			m_state = STATE_SACCADE;
			
			m_box1->show();
			m_box2->show();
			moveToFront(m_box1);
			moveToFront(m_box2);

			// Picks a random delay for the beep after initial fixation time is over
			// Because the delay timer starts with the saccade timer, the first delay is actually 10, then 100, and so forth
			// i.e. subtract 100 (length of the saccade cue) from the timeDelayChoices to get actual value of delay
			//int timeDelayChoices[8] = {110, 200, 300, 400, 500, 600, 700, 800 };
			//int timeDelayChoices[8] = { 110, 150, 200, 250, 300, 400, 500, 600 };
			//int timeDelayChoices[8] = { 150, 175, 200, 225, 250, 300, 450, 600 }; //NATALYA changed over the phone
            int timeDelayChoices[8] = {300, 400, 500, 550, 600, 650, 700, 800};
			int timeDelayId = rand() % 8;
			//int	timeDelayId = m_paramsFile->getInteger(CFG_DELAYTIME) //ZOE ADDED THIS 9/24; TRYING TO PUT DELAY IN PARAMS
			m_delayTime = timeDelayChoices[timeDelayId];
			
			//CEnvironment::Instance()->outputMessage("\nDelay ID: %i", timeDelayId);
			//CEnvironment::Instance()->outputMessage("\nDelay Time: %1f", m_delayTime);
			m_timerDelay.start(m_delayTime);
			TimeDelayON = m_timerExp.getTime();
			m_timerSaccCue.start(m_saccTime);
			TimeSaccadeON = m_timerExp.getTime();
			
			//CEnvironment::Instance()->outputMessage("\nDelay Time ON: %1f", TimeDelayON);
			//CEnvironment::Instance()->outputMessage("\nSaccade Time ON: %1f", TimeSaccadeON);

			if (SaccCueType == 0) {
				//CEnvironment::Instance()->outputMessage("TIME: %2f", m_timerSaccCue.getTime() - TimeSaccadeON);
				//m_neutralcue_sacc->show();
				totalNeutral++;
				totalTrials++;
				CEnvironment::Instance()->outputMessage("Prop. Neutral: %2f", totalNeutral / totalTrials);
				
			}
			// Left directional
			else if (SaccCueType == 1) {
				m_cue_sacc->degSetAngle(0);
				//m_cue_sacc->show();
				totalLeft++;
				totalTrials++;
				CEnvironment::Instance()->outputMessage("Prop. Left: %2f", totalLeft / totalTrials);
			}
			// Right directional
			else if (SaccCueType == 2) {
				m_cue_sacc->degSetAngle(180);
				//m_cue_sacc->show();
				totalRight++;
				totalTrials++;
				CEnvironment::Instance()->outputMessage("Prop. Right: %2f", totalRight / totalTrials);
			}
		}

		break;

		// STATE_SACCADE_TARGET_DELAY: starts the delay and saccade time, presents the target after delay timer expires,
		// hides target after target time expires and moves to STATE_MASK when both target and saccade timer expires
	case STATE_SACCADE:
			
			//CEnvironment::Instance()->outputMessage("STATE SACCADE");
			m_cue_fixation->show();
			
				// Saccade Cue Presentation
				if (!m_timerSaccCue.isExpired()) {
					if (SaccCueType == 0) {
						m_cue_fixation->hide();
						m_neutralcue_sacc->show();
					}
					else {
						m_cue_sacc->show();
					}
					
				}
				else if (m_timerSaccCue.isExpired()) {
					//CEnvironment::Instance()->outputMessage("EXPIRED");
					m_neutralcue_sacc->hide();
					m_cue_sacc->hide();
					m_cue_fixation->show();
					TimeSaccadeOFF = m_timerExp.getTime();
					m_state = STATE_DELAY;
					//CEnvironment::Instance()->outputMessage("\nSaccade Time OFF: %1f", TimeSaccadeOFF);
				}
			

		break;

	case STATE_DELAY:

		//CEnvironment::Instance()->outputMessage("STATE TARGET");
		m_cue_fixation->show();

		if (!m_timerDelay.isExpired()) {
			m_target1->hide();
			m_target2->hide();
		}
		else if (m_timerDelay.isExpired()) {
			TimeDelayOFF = m_timerExp.getTime();
			//CEnvironment::Instance()->outputMessage("\nDelay Time OFF: %1f", TimeDelayOFF);
			TimeTargetON = m_timerExp.getTime();
			//CEnvironment::Instance()->outputMessage("\nTarget Time ON: %1f", TimeTargetON);
			m_timerTarget.start(m_targetTime);
			m_state = STATE_TARGET;

		}

		break;

	case STATE_TARGET:

		//CEnvironment::Instance()->outputMessage("STATE TARGET");

		if (!m_timerTarget.isExpired()) {
			m_target1->show();
			m_target2->show();
			moveToFront(m_target1);
			moveToFront(m_target2);
		}

		else if (m_timerTarget.isExpired()) {
			m_target1->hide();
			m_target2->hide();
			TimeTargetOFF = m_timerExp.getTime();
			m_state = STATE_MASK;
			TimeMaskON = m_timerExp.getTime();
			m_timerMask.start(m_maskTime);
		}

		break;

		// STATE_MASK: placeholders are back as a mask before cue is presented
	case STATE_MASK:

		//CEnvironment::Instance()->outputMessage("STATE MASK");

		if (!m_timerMask.isExpired())
		{

		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Cue");
			m_cue_resp->pxSetPosition(cueX + Xlocation, cueY + Ylocation);
			moveToFront(m_cue_resp);
			m_cue_resp->show();
			TimeMaskOFF = m_timerExp.getTime();

			m_timerRespCue.start(m_respcueTime);
			m_state = STATE_CUE;
			TimeRespCueON = m_timerExp.getTime();
		}

		break;


		// STATE_CUE: present the response cue
	case STATE_CUE:

		if (!m_timerRespCue.isExpired())
		{
			moveToFront(m_cue_resp);
			m_cue_resp->show();
		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Response");
			TimeRespCueOFF = m_timerExp.getTime();
			m_state = STATE_RESPONSE;
			hideAllObjects();

			WAIT_RESPONSE = 1;
			m_timerResponse.start(m_responseTime);
			TimeResponseON = m_timerExp.getTime();
		}
		break;

		// STATE_RESPONSE: wait for subject response 
	case STATE_RESPONSE:

		if (((m_timerResponse.isExpired())) || (WAIT_RESPONSE == 0))
		{
			if (WAIT_RESPONSE == 1)
				CEnvironment::Instance()->outputMessage("Response not given");
			endTrial();
			saveData();
		}

		break;

	}}		
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventJoypad()
{
// activate the joypad only in the state calibration	

	if (m_state == STATE_TESTCALIBRATION) 
	{				

		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_UP)) // moving the cursor up
		{
			yPos = yPos + Increment; //position of the cross
		}
	
		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_DOWN)) // moving the cursor down
		{	
			yPos = yPos - Increment;
		}

		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_RGHT)) // moving the cursor to the right
		{
			xPos = xPos + Increment;

		}

		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_LEFT)) // moving the cursor to the left
		{
			xPos = xPos - Increment;
		
		}
		
		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_X)) // finalize the response
		{
				//CEnvironment::Instance()->outputMessage("Recalibration finalized");
				ResponseFinalize = 1; // click the left botton to finalize the response

		}
   }
	if (m_state == STATE_RESPONSE)
	{
		if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) |
			(CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) )
		{
			WAIT_RESPONSE = 0;
			totalResponses++;
			// get the time of the response here
			ResponseTime =  m_timerExp.getTime();
		
		// right press for rightward
		if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) )// target tilted R
		{					
			CEnvironment::Instance()->outputMessage("\nSubject's Response: Right");
			Response = 1;
		}

		if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) )// target tilted L
		{
			CEnvironment::Instance()->outputMessage("\nSubject's Response: Left");
			Response = 0;
		}

		if (CuedTargetOrientation == 0) {
			CEnvironment::Instance()->outputMessage("Target Orientation: Left");
		}
		else if (CuedTargetOrientation == 1) {
			CEnvironment::Instance()->outputMessage("Target Orientation: Right");
		}

		if (CuedTargetOrientation == Response)
		{
			Correct = 1;
			if (SaccCueType == 0) {
				neutralCorrect++;
			}
			totalCorrect++;
			CEnvironment::Instance()->outputMessage("Correct\n");
		}
		else
		{
			Correct = 0;
			CEnvironment::Instance()->outputMessage("Wrong\n");
		}
		
		if (SaccCueType == 0 ) {
			CEnvironment::Instance()->outputMessage("Neutral Correct: %2f", neutralCorrect/totalNeutral);
		}
		CEnvironment::Instance()->outputMessage("Prop Correct: %2f", totalCorrect/totalResponses);
;
		}


	}
}

void ExperimentBody::eventKeyboard(unsigned char key, int x, int y) {
	//CEnvironment::Instance()->outputMessage("key press %i %i", x, y);
	if (debug == 1 && m_state == STATE_RESPONSE) {
		
		if (key == 'd' || key == 'D') {
			WAIT_RESPONSE = 1;
			ResponseTime = m_timer.getTime();
			Response = 1;
		}
		else if (key == 'a' || key == 'A') {
			WAIT_RESPONSE = 1;
			ResponseTime = m_timer.getTime();
			Response = 0;
		}
		
	}
	if (key == 'g' || key == 'G') {
		ResponseFinalize = 1;
	}
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::gotoFixation()
{

    if (!(TestCalibration == 1))
        CEnvironment::Instance()->outputMessage("Trial Number: %d\n", (TrialNumber));


    hideAllObjects();


    if (TestCalibration == 1){
        m_state = STATE_TESTCALIBRATION;
        //CEnvironment::Instance()->outputMessage("Calibration trial");
        ResponseFinalize = 0;
        m_timerCheck.start(500);
		startTrial();
    }
    else{

        m_state = STATE_FIXATION;
        gate = 1;


		// if the response is not given Correct is set at 10
		Correct = 10;
		// list with the orientations of targets at all locations
		// pick orientation randomly (45 -> rightward 135-> leftward)
		int choices[2] = { 45, 135 };
		int id = rand() % 2;
		Target1Orientation = choices[id];
		id = rand() % 2;;
		Target2Orientation = choices[id];
		id = rand() % 2;
		m_target1->degSetAngle(Target1Orientation);
		m_target2->degSetAngle(Target2Orientation);

		// cue location expressed by quadrant
		
		int locations[2] = {1,2};		
		CueLocation = locations[rand() % 2];

		//CueLocation = 1;

		// Determine saccade cue location
		int arr[] = {0, 1, 2};
		int freq[] = {30, 35, 35};
		int n = sizeof(arr) / sizeof(arr[0]);
		SaccCueType = myRand(arr, freq, n);

		//CEnvironment::Instance()->outputMessage("%i", CueLocation);
		// define cue coordinates
		if (CueLocation == 1)
		{
			cueY = BoxSize;
			cueX = -TargetOffset;
			//CEnvironment::Instance()->outputMessage("Cued Location: 1");

			if (Target1Orientation == 45)
			{
				//CEnvironment::Instance()->outputMessage("Cued Target Orientation: Right");
				CuedTargetOrientation = 1;
			}
			else
			{
				//CEnvironment::Instance()->outputMessage("Cued Target Orientation: Left");
				CuedTargetOrientation = 0;
			}
		}
		if (CueLocation == 2)
		{
			cueY = BoxSize;
			cueX = TargetOffset;
			//CEnvironment::Instance()->outputMessage("Cued Location: 2");
			if (Target2Orientation == 45)
			{
				//CEnvironment::Instance()->outputMessage("Cued Target Orientation: Right");
				CuedTargetOrientation = 1;
			}
			else
			{
				//CEnvironment::Instance()->outputMessage("Cued Target Orientation: Left");
				CuedTargetOrientation = 0;
			}
		}
		
		CEnvironment::Instance()->outputMessage("Cued Location   | Sacc. Cue. Location");
		CEnvironment::Instance()->outputMessage("            %i               |              %i            ", CueLocation, SaccCueType);


		// start the trial
		WAIT_RESPONSE = 1;
		startTrial();
		m_timerExp.start();
		m_timer.start(1000);
		gate = 1;

	}
		
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::saveData()
{

		if (ResponseTime>0)
	    // give confrimation of response
		Beep(600,400);

		// time of the response (locked to the start of the trial)		
		storeTrialVariable("TimeFixationON", TimeFixationON); 
		storeTrialVariable("TimeFixationOFF", TimeFixationOFF);
		storeTrialVariable("TimeDelayON", TimeDelayON);
		storeTrialVariable("TimeDelayOFF", TimeDelayOFF);
		storeTrialVariable("TimeSaccCueON", TimeSaccadeON);
		storeTrialVariable("TimeSaccCueOFF", TimeSaccadeOFF);
		storeTrialVariable("TimeTargetON", TimeTargetON);	
		storeTrialVariable("TimeTargetOFF", TimeTargetOFF);
		storeTrialVariable("TimeMaskON", TimeMaskON);
		storeTrialVariable("TimeMaskOFF", TimeMaskOFF);
		storeTrialVariable("TimeRespCueON", TimeRespCueON);
		storeTrialVariable("TimeRespCueOFF", TimeRespCueOFF);
		storeTrialVariable("Correct", Correct);

		// event total time
		storeTrialVariable("FixationTime", m_fixationTime);
		storeTrialVariable("DelayTime", m_delayTime);
		storeTrialVariable("SaccTime", m_saccTime);
		storeTrialVariable("TargetTime", m_targetTime);
		storeTrialVariable("MaskTime", m_maskTime);
		storeTrialVariable("RespCueTime", m_respcueTime);
		
		// save target orientation
		storeTrialVariable("Target1Orientation", Target1Orientation);
		storeTrialVariable("Target2Orientation", Target2Orientation);
		// x location and y location of objects based on initial gaze position in px with offset added
		storeTrialVariable("XlocationPx", Xlocation);
		storeTrialVariable("YlocationPx", Ylocation);

		// dimensions targets
		storeTrialVariable("FixSize", FixSize); //px
		storeTrialVariable("BoxSize", BoxSize);
		storeTrialVariable("TSizeX", TSizeX);
		storeTrialVariable("TSizeY", TSizeY);

		// type of saccade cue 
		storeTrialVariable("SaccCueType", SaccCueType); // 0 = neutral 1 = directional R 2 = directional L

		// save cue location
		storeTrialVariable("CueLocation", CueLocation);	
		storeTrialVariable("CuedTargetOrientation", CuedTargetOrientation);

		// save target rgb value
		storeTrialVariable("TargetRGB", m_paramsFile->getFloat(CFG_RGB_VALUE));
		// save target size in px
		storeTrialVariable("TSizeXpx", TSizeX);
		storeTrialVariable("TSizeYpx", TSizeY);
	
		storeTrialVariable("Response", Response);				
		storeTrialVariable("ResponseTime", ResponseTime);
		storeTrialVariable("TargetOffsetpx", TargetOffset);//px
		
		storeTrialVariable("RefreshRate", m_paramsFile->getFloat(CFG_REFRESH_RATE));
		storeTrialVariable("Xres", m_paramsFile->getFloat(CFG_X_RES));
		storeTrialVariable("Yres", m_paramsFile->getFloat(CFG_Y_RES));
		
		
		storeTrialVariable("Subject_Name", m_paramsFile->getString(CFG_SUBJECT_NAME));
		
		// save information about the test calibration
		storeTrialVariable("TestCalibration", TestCalibration);
		storeTrialVariable("xoffset", xshift);
		storeTrialVariable("yoffset", yshift); //px

		saveTrial("./Data/" + m_paramsFile->getString(CFG_SUBJECT_NAME));

		
		// keep track of the test calibration trials
		m_numTestCalibration++;
		
		// recalibration active at each trial (here set at 1)
		if (m_numTestCalibration == 1) 
	    {
			xPos = 0;
			yPos = 0;
			TestCalibration = 1;
			ResponseFinalize = 0;
			m_numTestCalibration = 0;
			m_timerCheck.start(100);
			m_whitecross->pxSetPosition(0,0);
			m_whitecross->show();
	    }

		
		TrialNumber++;
		CEnvironment::Instance()->outputMessage("-----------------------------------------------------");
		gotoFixation();
		
}
///////////////////////////////////////////////////////////////////////////////////
std::string ExperimentBody::int2string(int x){

	stringstream temps;
	temps << x;
	return temps.str();
}

////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
// random number generator based on relative frequency

// Utility function to find ceiling of r in arr[l..h]
int ExperimentBody::findCeil(std::vector<int> arr, int r, int l, int h)
{
	int mid;
	while (l < h)
	{
		mid = l + ((h - l) >> 1);  // Same as mid = (l+h)/2
		(r > arr[mid]) ? (l = mid + 1) : (h = mid);
	}
	return (arr[l] >= r) ? l : -1;
}
// The main function that returns a random number from arr[] according to
// distribution array defined by freq[]. n is size of arrays.
int ExperimentBody::myRand(int arr[], int freq[], int n)
{
	srand(time(NULL));
	// Create and fill prefix array
	std::vector<int> prefix(n, 0);
	int i;
	prefix[0] = freq[0];
	for (i = 1; i < n; ++i)
		prefix[i] = prefix[i - 1] + freq[i];

	// prefix[n-1] is sum of all frequencies. Generate a random number
	// with value from 1 to this sum
	int r = (rand() % prefix[n - 1]) + 1;

	// Find index of ceiling of r in prefix arrat
	int indexc = findCeil(prefix, r, 0, n - 1);
	return arr[indexc];
}