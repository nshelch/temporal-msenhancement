
#define _EXPERIMENTBODY_H

#include "emil/emil.hpp"
#include <fstream>


/*#pragma once

#include "emil/EMIL.hpp"
#include <queue>
#include <io.h>*/

using namespace std;

class ExperimentBody: public CExperiment
{
public:
	ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params);

	/// Standard event handlers
	void initialize();
	void finalize();

	void eventRender(unsigned int FrameCount, CEOSData* Samples);
	void eventKeyboard(unsigned char key, int x, int y);
	void eventJoypad();

private:

	int findCeil(std::vector<int> arr, int r, int l, int h);
	int myRand(int arr[], int freq[], int n);
	std::string ExperimentBody::int2string(int x);
	int ExperimentBody::string2int(std::string x);
	int temp_int;

	// Current experiment state
	enum STATE {
		STATE_LOADING,
		STATE_TESTCALIBRATION,
		STATE_FIXATION,
		STATE_SACCADE,
		STATE_DELAY,
		STATE_TARGET,
		STATE_CUE,
		STATE_MASK,
		STATE_RESPONSE,
	};

	STATE m_state;

	bool proceedToNextState;
	void gotoFixation();
    void saveData();

	// Configuration file
	CCfgFile* m_paramsFile;

	CSolidPlane* m_target1;
	CSolidPlane* m_target2;
	CSolidPlane* m_cue_fixation;
	CImagePlane* m_cue_resp;
	CImagePlane* m_cue_sacc;
	CImagePlane* m_neutralcue_sacc;;
	CSolidPlane* m_box1;
	CSolidPlane* m_box2;


	// Stimuli for the test calibration
	CSolidPlane* m_blackcross;
	CSolidPlane* m_whitecross;

	int FixSize;
	int BoxSize;
	int TSizeX;
	int TSizeY;
	int SaccCueType;
	int TargetOffset;

	// timers
	CTimer m_timer;
	CTimer m_timerCheck;
	CTimer m_timerExp;
	CTimer m_timerFixation;
	CTimer m_timerResponse; 
	CTimer m_timerTarget;
	CTimer m_timerRespCue;
	CTimer m_timerMask;
	CTimer m_timerSaccCue;
	CTimer m_timerDelay;
	
	float TimeFixationON;
	float TimeFixationOFF;
	float TimeSaccadeON;
	float TimeSaccadeOFF;
	float TimeDelayON;
	float TimeDelayOFF;
	float TimeTargetON;
	float TimeTargetOFF;
	float TimeMaskON;
	float TimeMaskOFF;
	float TimeRespCueON;
	float TimeRespCueOFF;
	float TimeResponseON;

	float m_targetTime;
	float m_fixationTime;
	float m_saccTime;
	float m_maskTime;
	float m_respcueTime;
	float m_responseTime; // how long the subject has to respond (set in params)
	float m_delayTime;

	int Increment;
	int m_numTestCalibration;
	int ResponseFinalize;
	int TestCalibration;
	int WAIT_RESPONSE;
	int gate;
	int TrialNumber;
	int debug;
	int Correct;
	int Response;
	
	float xPos;
	float yPos;
	float xshift;
	float yshift;

	float totalNeutral;
	float totalTrials;
	float totalLeft;
	float totalRight;
	float totalCorrect;
	float totalResponses;
	float neutralCorrect;
	float validCorrect;

	int CueLocation;
	int CuedTargetOrientation;
	int Target1Orientation;
	int Target2Orientation;
	float Xlocation;
	float Ylocation;
	float ResponseTime; // the time at which the subject made a response


	float cueX;
	float cueY;

};
