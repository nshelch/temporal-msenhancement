function dataStruct = discardedTrialsStruct()

dataStruct.microsaccade = 0;
dataStruct.gaze_off_center = 0;
dataStruct.target_distance = 0;
dataStruct.saccades = 0;
dataStruct.no_response = 0;
dataStruct.blinks = 0;
dataStruct.no_track = 0;
dataStruct.no_response = 0;
dataStruct.too_short = 0;
dataStruct.late_ms = 0;
dataStruct.multiple_ms = 0;

end