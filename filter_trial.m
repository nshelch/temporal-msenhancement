function [output, dt] = filter_trial(trial, dt, params)

if check_blinks(trial, params)
    dt.blinks = dt.blinks + 1;
    output = 0;
elseif check_no_tracks(trial, params)
    dt.no_track = dt.no_track + 1;
    output = 0;
elseif ~check_trial_duration(trial, params) % returns 0 if trial duration is short
    dt.too_short = dt.too_short + 1;
    output = 0;
elseif check_saccades(trial, params)
    dt.saccades = dt.saccades + 1;
    output = 0;
elseif check_no_response(trial)
    dt.no_response = dt.no_response + 1;
    output = 0;
elseif check_invalid(trial)
    dt.manual_discard = dt.manual_discard + 1;
    output = 0;
else
    output = 1;
end

end

function output = check_blinks(trial, params)
fix_on = trial.TimeFixationON - 100;
target_on = trial.TimeTargetON + 400;
if params.DDPI
    output = isIntersectedIn(fix_on/params.sampleConversion, (target_on - fix_on)/params.sampleConversion, trial.blinks);
else
    output = isIntersectedIn(fix_on, (target_on - fix_on), trial.blinks);
end
end

function output = check_no_tracks(trial, params)
fix_on = trial.TimeFixationON - 100;
target_on = trial.TimeTargetON + 400;
if params.DDPI
    output = isIntersectedIn(fix_on/params.sampleConversion, (target_on - fix_on)/params.sampleConversion, trial.notracks);
else
    output = isIntersectedIn(fix_on, (target_on - fix_on), trial.notracks);
end
end

function output = check_saccades(trial, params)
fix_on = trial.TimeFixationON;
resp_cue_on = trial.TimeRespCueON;
if params.DDPI
    output = isIntersectedIn(fix_on/params.sampleConversion, (resp_cue_on - fix_on)/params.sampleConversion, trial.saccades);
else
    output = isIntersectedIn(fix_on, (resp_cue_on - fix_on), trial.saccades);
end
end

function output = check_trial_duration(trial, params)
if params.DDPI
    output = trial.TimeRespCueON < (length(trial.x.position) * params.sampleConversion);
else
    output = trial.TimeRespCueON < length(trial.x.position);
end
end

function output = check_no_response(trial)
output = trial.Correct > 2;
end

function output = check_invalid(trial)
output = ~isempty(trial.invalid.start);
end
