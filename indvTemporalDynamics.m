function ft = indvTemporalDynamics(ft, nBins)

for trialType = {'cong','incong'}
    subjResp = double(ft.subj_resp(ft.(trialType{1}).id));
    targetOrientation = ft.orientation_cued_target(ft.(trialType{1}).id);
    respTimes = ft.response_time(ft.(trialType{1}).id);
    msLatency = ft.(trialType{1}).ms_latency;
    [~, ~, thresh, ~] = PartitionEqualGroups(ft.(trialType{1}).ms_latency', nBins);
    
    for threshIdx = 1:length(thresh) - 1
        fname = sprintf('%s_bin%i', trialType{1}, threshIdx);
        lowerThresh = thresh(threshIdx);
        upperThresh = thresh(threshIdx + 1);
        
        ft.temp_dynamics.trialIdx.(fname) = find(msLatency > lowerThresh & msLatency <= upperThresh);
        validTrials = ft.temp_dynamics.trialIdx.(fname);
        [ft.temp_dynamics.(sprintf('%s_dprime', trialType{1}))(threshIdx), ~, ...
            ft.temp_dynamics.(sprintf('%s_ci_dprime', trialType{1}))(threshIdx), ~, ~] = ...
            CalculateDprime_2(subjResp(validTrials), targetOrientation(validTrials));
        
        ft.temp_dynamics.(sprintf('%s_resp_time', trialType{1}))(threshIdx) = nanmean(respTimes(validTrials));
        ft.temp_dynamics.(sprintf('%s_se_resp_time', trialType{1}))(threshIdx) = nanstd(respTimes(validTrials));
        
        ft.temp_dynamics.(sprintf('%s_n', trialType{1}))(threshIdx) = length(validTrials);
    end
end

for threshIdx = 1:length(thresh) - 1
    ft.temp_dynamics.ms_latency(threshIdx) = ...
        mean([ft.cong.ms_latency(ft.temp_dynamics.trialIdx.(sprintf('cong_bin%i', threshIdx))), ...
        ft.incong.ms_latency(ft.temp_dynamics.trialIdx.(sprintf('incong_bin%i', threshIdx)))]);
    ft.temp_dynamics.se_ms_latency(threshIdx) = ...
        nanstd([ft.cong.ms_latency(ft.temp_dynamics.trialIdx.(sprintf('cong_bin%i', threshIdx))), ...
        ft.incong.ms_latency(ft.temp_dynamics.trialIdx.(sprintf('incong_bin%i', threshIdx)))]);    
end

ft.temp_dynamics.cong_incong_delta_dprime = ft.temp_dynamics.cong_dprime - ft.temp_dynamics.incong_dprime;
ft.temp_dynamics.cong_incong_delta_resp_time = ft.temp_dynamics.cong_resp_time - ft.temp_dynamics.incong_resp_time;

ft.temp_dynamics.cong_neutral_delta_dprime = ft.temp_dynamics.cong_dprime - ft.neutral.dprime;
ft.temp_dynamics.cong_neutral_delta_resp_time = ft.temp_dynamics.cong_resp_time - mean(ft.neutral.response_time);

ft.temp_dynamics.incong_neutral_delta_dprime = ft.temp_dynamics.incong_dprime - ft.neutral.dprime;
ft.temp_dynamics.incong_neutral_delta_resp_time = ft.temp_dynamics.incong_resp_time - mean(ft.neutral.response_time);

end