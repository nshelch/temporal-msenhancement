function temporalDynamicsFigure(ft)

figure;
ht = plot([0, 0], [-5, 5], 'r', 'LineWidth', 2)
hold on
errorbar(ft.temp_dynamics.ms_latency, ft.temp_dynamics.cong_incong_delta_dprime, ft.temp_dynamics.se_ms_latency, ...
    'horizontal', 'k','LineStyle', '-','LineWidth', 1.5)
plot(ft.temp_dynamics.ms_latency, ft.temp_dynamics.cong_incong_delta_dprime, ...
    'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 8);
hold on 
hn = plot([-1100 650], ...
    [ft.cong_no_ms.dprime - ft.incong_no_ms.dprime, ft.cong_no_ms.dprime-ft.incong_no_ms.dprime],'--b')
ylabel("\Delta Sensitivity [d']")
xlabel('Microsaccade Latency from Target Onset')
ylim([floor(min(ft.temp_dynamics.cong_incong_delta_dprime)) ceil(max(ft.temp_dynamics.cong_incong_delta_dprime))])
set(gca, 'FontSize', 15)

for t = 1:length(ft.temp_dynamics.ms_latency)
    if t > 1 && ft.temp_dynamics.cong_incong_delta_dprime(t - 1) > ft.temp_dynamics.cong_incong_delta_dprime(t)
        text(ft.temp_dynamics.ms_latency(t) - 35, ft.temp_dynamics.cong_incong_delta_dprime(t) - .1, sprintf('n = %i', round(mean([ft.temp_dynamics.cong_n(t),ft.temp_dynamics.incong_n(t)]))), ...
            'FontSize', 12, 'FontWeight', 'bold')
    elseif t > 1 && ft.temp_dynamics.cong_incong_delta_dprime(t - 1) < ft.temp_dynamics.cong_incong_delta_dprime(t)
        text(ft.temp_dynamics.ms_latency(t) - 35, ft.temp_dynamics.cong_incong_delta_dprime(t) + .1, sprintf('n = %i', round(mean([ft.temp_dynamics.cong_n(t),ft.temp_dynamics.incong_n(t)]))), ...
            'FontSize', 12, 'FontWeight', 'bold')
    elseif t == 1
        text(ft.temp_dynamics.ms_latency(t) - 35, ft.temp_dynamics.cong_incong_delta_dprime(t) + .1, sprintf('n = %i', round(mean([ft.temp_dynamics.cong_n(t),ft.temp_dynamics.incong_n(t)]))), ...
            'FontSize', 12, 'FontWeight', 'bold')
    end
end

legend([ht hn], {'Target On','Fix. Cong. - Fix. Incong.'}, 'Location', 'Best', 'FontSize', 10, 'FontWeight','bold')
end