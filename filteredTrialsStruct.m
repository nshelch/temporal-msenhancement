function dataStruct = filteredTrialsStruct()

dataStruct.perf = NaN;
dataStruct.id = NaN;
dataStruct.gaze_pos = NaN;
dataStruct.amp = NaN;
dataStruct.ms_latency = NaN;
dataStruct.dist_from_target = {NaN};
dataStruct.landing_error = NaN;
dataStruct.avg_perf = NaN;

end