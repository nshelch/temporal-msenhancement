function y = nansem(data)

y = nanstd(data/sqrt(length(data)));

end

